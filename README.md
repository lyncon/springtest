# Spring Test
First spring project for testing new features and architectures. This project currentily uses:
- Lombok
- Spring Boot
- Spring Web
- Spring Data JPA
- Spring Security

## Project Scope

This project aims to create a rest api for bookstore. 
the resources that need to be maintained are: 

#### Book
 - Title
 - Authors
 - Publisher
 - Edition
 
#### Author
 - Name
 - Genre
 - Books

#### Publisher
  - Name
  - Books

There are 2 roles of users with different authorities, these 2 roles of users contain the same information:
#### User
 - User name (login)
 - Complete Name
 - Password
 - Enabled
 - E-mail
 
#### Client
Clients can simulate a purchase by deleting books from the system.
#### Manager 
Managers can do all CRUD actions for books,publishers and authors 

Things to do

 - Implement Method Security Authorization
 - Implement JWT Authentication
 - Generate Swagger documentation
 - Implement password recovery
 - Learn Unit Tests
 - Standardize Response structure
 - Implement Error Handling


---
Author: Lyncon Baez

License: MIT   