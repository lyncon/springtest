package br.com.lyncon.springPlay.security.filters;

import br.com.lyncon.springPlay.security.JwtToken;
import br.com.lyncon.springPlay.security.JwtUtil;
import br.com.lyncon.springPlay.security.authentications.UsernamePasswordAuthentication;
import br.com.lyncon.springPlay.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CustomUsernamePasswordAuthenticationFilter extends OncePerRequestFilter {
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;
    private final ObjectMapper objectMapper;

    @Autowired
    public CustomUsernamePasswordAuthenticationFilter(AuthenticationManager authenticationManager, JwtUtil jwtUtil, ObjectMapper objectMapper) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.objectMapper = objectMapper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Authentication auth = new UsernamePasswordAuthentication(username,password);
        Authentication authAuthenticated = authenticationManager.authenticate(auth);

        JwtToken jwtToken = jwtUtil.createToken((User) authAuthenticated.getPrincipal());
        response.addHeader("Authorization","Bearer "+jwtToken.getToken());
        response.getWriter().append(objectMapper.writeValueAsString(jwtToken));
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return !request.getServletPath().equals("/login");
    }


}
