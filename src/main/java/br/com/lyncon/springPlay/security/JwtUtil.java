package br.com.lyncon.springPlay.security;

import br.com.lyncon.springPlay.user.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;


@Component
public class JwtUtil {


    private String secret = "rzvxVNiQc-dRNL-cHXjTNXZDwOKdlGQffruLYCuL48Nl_BVGYMmKylMdaX4QB7RTFcmtshHIKTldOE1EStLA";

    private Long expires = 5184000L;

    private Claims parseToken(String token) {
        return Jwts.parser()
                .setSigningKey(this.secret)
                .parseClaimsJws(token)
                .getBody();
    }

    public JwtToken createToken(User authenticatedUser) {
        Claims claims = Jwts.claims().setSubject(authenticatedUser.getUsername());
        claims.put("roles", authenticatedUser.getAuthorities());
        claims.put("id", authenticatedUser.getId());
        claims.put("name", authenticatedUser.getName());

        Instant expirationTime = Instant.now().plusSeconds(expires);

        Date expirationDate = Date.from(expirationTime);

        return new JwtToken(Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secret)
                .setExpiration(expirationDate)
                .compact());
    }

}

