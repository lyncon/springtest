package br.com.lyncon.springPlay.book.author;

import br.com.lyncon.springPlay.book.Book;
import br.com.lyncon.springPlay.book.BookDto;
import br.com.lyncon.springPlay.book.BookMinimalDto;
import lombok.Data;
import org.modelmapper.ModelMapper;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class AuthorDto {
    private Long id;
    private String name;
    private String genre;
    private Set<BookMinimalDto> books;

    public static AuthorDto from(Author author) {
        ModelMapper modelMapper =  new ModelMapper();
        AuthorDto authorDto = modelMapper.map(author,AuthorDto.class);
        return authorDto;
    }
}
