package br.com.lyncon.springPlay.book.author;

import lombok.Data;
import org.modelmapper.ModelMapper;

@Data
public class AuthorMinimalDto {
    private Long id;
    private String name;
    private String genre;

    public static AuthorMinimalDto from(Author author) {
        ModelMapper modelMapper =  new ModelMapper();
        return modelMapper.map(author,AuthorMinimalDto.class);
    }
}
