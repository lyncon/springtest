package br.com.lyncon.springPlay.book;

import lombok.Data;
import org.modelmapper.ModelMapper;

@Data
public class BookMinimalDto {
    private Long id;
    private String title;
    private String edition;

    public static BookMinimalDto from(Book book) {
        ModelMapper modelMapper =  new ModelMapper();
        return modelMapper.map(book,BookMinimalDto.class);
    }
}
