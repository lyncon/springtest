package br.com.lyncon.springPlay.book.publisher;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublisherService {

    private final PublisherRepository publisherRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public PublisherService(PublisherRepository publisherRepository, ModelMapper modelMapper) {
        this.publisherRepository = publisherRepository;
        this.modelMapper = modelMapper;
    }

    public PublisherDto create(PublisherDto publisherDtoRequest) {
        Publisher publisherEntity = modelMapper.map(publisherDtoRequest,Publisher.class);
        publisherRepository.save(publisherEntity);
        return modelMapper.map(publisherEntity,PublisherDto.class);
    }
}
