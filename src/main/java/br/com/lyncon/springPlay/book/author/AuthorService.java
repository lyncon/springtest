package br.com.lyncon.springPlay.book.author;

import br.com.lyncon.springPlay.book.Book;
import br.com.lyncon.springPlay.book.BookService;
import br.com.lyncon.springPlay.exceptions.InvalidRequestException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;
    private final BookService bookService;
    private final ModelMapper modelMapper;
    @Autowired
    public AuthorService(AuthorRepository authorRepository, BookService bookService, ModelMapper modelMapper) {
        this.authorRepository = authorRepository;
        this.bookService = bookService;
        this.modelMapper = modelMapper;
    }

    public AuthorDto findById(Long id) {
        Author authorEntity  = this.checkExistenseAndReturn(id);
        return modelMapper.map(authorEntity,AuthorDto.class);
    }

    @Transactional
    public AuthorDto addAuthor(Long id,AuthorMinimalDto authorDto) {
        Book book = bookService.checkExistenseAndReturn(id);
        Author author = null;
        if (authorDto.getId() != null) {
            author = this.checkExistenseAndReturn(authorDto.getId());
            author.getBooks().add(book);
        } else {
            Author tempAuthor = modelMapper.map(authorDto,Author.class);
            Set<Book> books = new HashSet<>();
            tempAuthor.setBooks(books);
            author = authorRepository.save(tempAuthor);
            author.getBooks().add(book);
        }
        book.getAuthors().add(author);
        return modelMapper.map(author,AuthorDto.class);
    }

    public Author checkExistenseAndReturn(Long id) {
        Optional<Author> authorContainer = authorRepository.findById(id);
        if (!authorContainer.isPresent()) {
            throw new InvalidRequestException("Author not Found");
        }
        return authorContainer.get();
    }

    public Set<AuthorMinimalDto> findByBookId(Long bookId) {
        Book book = bookService.checkExistenseAndReturn(bookId);
        Set<Author> authors = authorRepository.findByBooks(book);
        return authors.stream().map(AuthorMinimalDto::from).collect(Collectors.toSet());
    }

    public Set<AuthorMinimalDto> findAll() {
        List<Author> authorList = authorRepository.findAll();
        return authorList.stream().map(AuthorMinimalDto::from).collect(Collectors.toSet());
    }

    public AuthorDto update(AuthorMinimalDto authorRequestDto) {
        Author oldAuthor = this.checkExistenseAndReturn(authorRequestDto.getId());
        Author newAuthor = modelMapper.map(authorRequestDto,Author.class);
        newAuthor.setBooks(oldAuthor.getBooks());
        authorRepository.save(newAuthor);
        return modelMapper.map(newAuthor,AuthorDto.class);
    }

    public void deleteById(Long id) {
        Author author = checkExistenseAndReturn(id);
        author.clearBooks();
        authorRepository.save(author);
        authorRepository.delete(author);
    }
}
