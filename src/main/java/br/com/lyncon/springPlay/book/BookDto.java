package br.com.lyncon.springPlay.book;

import br.com.lyncon.springPlay.book.author.AuthorDto;
import br.com.lyncon.springPlay.book.author.AuthorMinimalDto;
import br.com.lyncon.springPlay.book.publisher.Publisher;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class BookDto {
    private Long id;
    private String title;
    private String edition;
    private Set<AuthorMinimalDto> authors;
    private Publisher publisher;

    public static BookDto from(Book book) {
        ModelMapper modelMapper =  new ModelMapper();

        BookDto bookDto = modelMapper.map(book,BookDto.class);

        return bookDto;
    }
}
