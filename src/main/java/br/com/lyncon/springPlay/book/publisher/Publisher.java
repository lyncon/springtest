package br.com.lyncon.springPlay.book.publisher;

import br.com.lyncon.springPlay.book.Book;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Publisher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String cnpj;
    @OneToMany(mappedBy = "publisher",fetch = FetchType.LAZY)
    private Set<Book> booksPublished;

}
