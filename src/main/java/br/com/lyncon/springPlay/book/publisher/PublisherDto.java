package br.com.lyncon.springPlay.book.publisher;

import lombok.Data;

@Data
public class PublisherDto {
    private Long id;
    private String name;
    private String cnpj;

}
