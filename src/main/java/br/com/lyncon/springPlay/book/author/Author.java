package br.com.lyncon.springPlay.book.author;

import br.com.lyncon.springPlay.book.Book;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id","name"})
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String genre;
    @ManyToMany(mappedBy = "authors")
    private Set<Book> books;

    public void clearBooks() {
        this.books.forEach(book -> {
            this.getBooks().remove(book);
            book.getAuthors().remove(this);
        });

    }

}
