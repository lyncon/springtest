package br.com.lyncon.springPlay.book.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/authors")
public class AuthorController {
    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public ResponseEntity<Set<AuthorMinimalDto>> findAll() {
        Set<AuthorMinimalDto> authors = authorService.findAll();
        return ResponseEntity.ok(authors);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody AuthorMinimalDto authorRequestDto) {
        authorRequestDto.setId(id);
        AuthorDto authorResponseDto = authorService.update(authorRequestDto);
        return ResponseEntity.ok(authorResponseDto);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        AuthorDto authorDto = authorService.findById(id);
        return ResponseEntity.ok(authorDto);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        authorService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
