package br.com.lyncon.springPlay.book;

import br.com.lyncon.springPlay.book.author.Author;
import br.com.lyncon.springPlay.book.author.AuthorDto;
import br.com.lyncon.springPlay.book.author.AuthorMinimalDto;
import br.com.lyncon.springPlay.book.author.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;
    private final AuthorService authorService;

    @Autowired
    public BookController(BookService bookService, AuthorService authorService) {
        this.bookService = bookService;
        this.authorService = authorService;
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody BookMinimalDto bookDtoRequest) {
        BookDto bookDtoRespose = bookService.create(bookDtoRequest);
        return ResponseEntity.ok(bookDtoRespose);
    }

    @GetMapping
    public ResponseEntity<List<BookDto>> findAll() {
        List<BookDto> books = bookService.findAll();
        return ResponseEntity.ok(books);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        BookDto bookReponseDto = bookService.findById(id);
        return ResponseEntity.ok(bookReponseDto);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<?> update(@PathVariable Long id,@RequestBody BookMinimalDto bookRequestDto) {
        bookRequestDto.setId(id);
        BookDto bookResponseDto = bookService.update(bookRequestDto);
        return ResponseEntity.ok(bookResponseDto);
    }
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        bookService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    /*
     * Authors Mapping
     */
    @PostMapping(path = "/{bookId}/authors")
    public ResponseEntity<?> addAuthor(@PathVariable Long bookId, @RequestBody AuthorMinimalDto authorRequestDto) {
        AuthorDto authorDto = authorService.addAuthor(bookId,authorRequestDto);
        return ResponseEntity.ok(authorDto);
    }

    @GetMapping(path = "/{bookId}/authors")
    public ResponseEntity<?> getAuthors(@PathVariable Long bookId) {
        Set<AuthorMinimalDto> authorsDto= authorService.findByBookId(bookId);
        return ResponseEntity.ok(authorsDto);
    }

    /*
    * Publisher Mapping
     */
    @PostMapping(path = "/{bookId}/publisher")
    public ResponseEntity<?> addPublisher(@PathVariable Long bookId) {

        return ResponseEntity.ok().build();
    }


}
