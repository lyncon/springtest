package br.com.lyncon.springPlay.book;

import br.com.lyncon.springPlay.exceptions.InvalidRequestException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookService {
    private final ModelMapper modelMapper;
    private final BookRepository bookRepository;

    @Autowired
    public BookService(ModelMapper modelMapper, BookRepository bookRepository) {
        this.modelMapper = modelMapper;
        this.bookRepository = bookRepository;
    }

    public BookDto create(BookMinimalDto bookDto) {
        if(bookDto.getId() != null) {
            throw new InvalidRequestException("Book already have an id");
        }
        Book book = modelMapper.map(bookDto, Book.class);
        Book bookCreated = bookRepository.save(book);
        return BookDto.from(bookCreated);
    }

    public List<BookDto> findAll() {
        List<Book> books = bookRepository.findAll();
        return books.stream().map(BookDto::from).collect(Collectors.toList());
    }

    public Book checkExistenseAndReturn(Long id) {
        Optional<Book> bookContainer = bookRepository.findById(id);
        if (!bookContainer.isPresent()) {
            throw new InvalidRequestException("Book not found");
        }
        return bookContainer.get();
    }

    public BookDto findById(Long id) {
       Book book = this.checkExistenseAndReturn(id);
       return modelMapper.map(book,BookDto.class);
    }

    @Transactional
    public BookDto update(BookMinimalDto bookRequestDto) {
        Book book = this.checkExistenseAndReturn(bookRequestDto.getId());
        book.setEdition(bookRequestDto.getEdition());
        book.setTitle(bookRequestDto.getTitle());
        return modelMapper.map(book,BookDto.class);
    }

    public void deleteById(Long id) {
        Book book = this.checkExistenseAndReturn(id);
        bookRepository.delete(book);
    }
}
