package br.com.lyncon.springPlay.book.author;

import br.com.lyncon.springPlay.book.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface AuthorRepository extends JpaRepository<Author,Long> {
    Set<Author> findByBooks(Book book);
}
