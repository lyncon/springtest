package br.com.lyncon.springPlay.book;

import br.com.lyncon.springPlay.book.author.Author;
import br.com.lyncon.springPlay.book.publisher.Publisher;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id","title"})
public class Book {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String edition;
    @ManyToMany
    private Set<Author> authors;
    @ManyToOne
    private Publisher publisher;

}
