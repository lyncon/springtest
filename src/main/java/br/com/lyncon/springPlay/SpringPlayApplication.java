package br.com.lyncon.springPlay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringPlayApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringPlayApplication.class, args);
	}
}
