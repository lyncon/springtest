package br.com.lyncon.springPlay.user;

import br.com.lyncon.springPlay.exceptions.InvalidRequestException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Autowired
    public UserService(ModelMapper modelMapper, PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public UserDto create(UserDto user) {
        if (user.getId() != null) {
            throw new InvalidRequestException("User already have an id, use update method to update this user");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User userEntity = modelMapper.map(user,User.class);
        userRepository.save(userEntity);
        return UserDto.from(userEntity);
    }

    public UserDto findByUserName(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        User userEntity = userOptional.orElseThrow(() -> new InvalidRequestException("User name not Found"));
        return modelMapper.map(userEntity,UserDto.class);
    }
}
