package br.com.lyncon.springPlay.user;

import br.com.lyncon.springPlay.common.enums.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.modelmapper.ModelMapper;

import java.util.Set;

@Setter
@Getter
@ToString
public class UserDto {
    private Long id;
    private String username;
    private String password;
    private String email;
    private String name;
    private Set<Role> roles;

    public static UserDto from(User user) {
        ModelMapper modelMapper =  new ModelMapper();
        return modelMapper.map(user,UserDto.class);
    }

}
